from turtle import Turtle
from unittest.mock import mock_open, patch
from unittest.mock import MagicMock

from snake_game.scoreboard import Scoreboard

ALIGNMENT = "center"
FONT = ("Courier", 24, "normal")


def test_update_scoreboard():
    scoreboard = Scoreboard()
    scoreboard.write = MagicMock()
    scoreboard.score = 10
    scoreboard.high_score = 20
    scoreboard.update_scoreboard()
    scoreboard.write.assert_called_once_with(
        f"Score: {scoreboard.score} High score : {scoreboard.high_score}",
        align="center",
        font=("Courier", 24, "normal")
    )

def test_reset():
    scoreboard = Scoreboard()
    scoreboard.score = 10
    scoreboard.high_score = 20
    scoreboard.reset()
    assert scoreboard.score == 0
    assert scoreboard.high_score == 20


def test_increase_score():
    scoreboard = Scoreboard()
    initial_score = scoreboard.score
    scoreboard.increase_score()
    assert scoreboard.score == initial_score + 1


def test_score_update_on_high_score():
    scoreboard = Scoreboard()
    scoreboard.score = 10
    scoreboard.high_score = 5
    with patch("builtins.open", mock_open(read_data="5")):
        scoreboard.reset()
    assert scoreboard.score == 0
    assert scoreboard.high_score == 10

