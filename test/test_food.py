from snake_game.food import Food


def test_food_refresh():
    food = Food()
    food.refresh()
    assert -280 <= food.xcor() <= 280
    assert -280 <= food.ycor() <= 280
