from snake_game.snake import Snake, STARTING_POSITIONS, MOVE_DISTANCE, UP, DOWN, LEFT, RIGHT


def test_create_snake():
    snake_san = Snake()
    assert len(snake_san.segments) == 3
    assert snake_san.head.position() == STARTING_POSITIONS[0]
    assert snake_san.segments[1].position() == STARTING_POSITIONS[1]
    assert snake_san.segments[2].position() == STARTING_POSITIONS[2]


def test_add_segment():
    snake = Snake()
    initial_length = len(snake.segments)
    snake.add_segment((0, 20))
    assert len(snake.segments) == initial_length + 1
    assert snake.segments[-1].position() == (0, 20)


def test_extend():
    snake = Snake()
    initial_length = len(snake.segments)
    snake.extend()
    assert len(snake.segments) == initial_length + 1
    assert snake.segments[-1].position() == STARTING_POSITIONS[-1]


def test_move():
    snake = Snake()
    initial_head_pos = snake.head.position()
    snake.move()
    assert snake.head.position() == (initial_head_pos[0] + MOVE_DISTANCE, initial_head_pos[1])


def test_reset():
    snake = Snake()
    snake.reset()
    assert len(snake.segments) == 3
    assert snake.head.position() == STARTING_POSITIONS[0]
    assert snake.segments[1].position() == STARTING_POSITIONS[1]
    assert snake.segments[2].position() == STARTING_POSITIONS[2]


def test_direction_up():
    snake = Snake()
    snake.up()
    assert snake.head.heading() == UP


def test_direction_down():
    snake = Snake()
    snake.down()
    assert snake.head.heading() == DOWN


def test_direction_left():
    snake = Snake()
    snake.head.setheading(UP)
    snake.left()
    assert snake.head.heading() == LEFT


def test_direction_right():
    snake = Snake()
    snake.right()
    assert snake.head.heading() == RIGHT
